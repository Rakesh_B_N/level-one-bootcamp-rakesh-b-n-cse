//Write a program to add two user input numbers using one function.
#include <stdio.h>
int main() {
	float a , b;
  printf("Enter two numbers : ");
  scanf("%f %f" , &a , &b);
  printf("%0.2f\n" , a + b ) ;
	return 0;
}