//Write a program to add two user input numbers using one function.

#include <stdio.h>
float sum_of_two_numbers(float a ,float b);
void printer(float x, float a, float b);
float scanner();

int main() 
{
	float a, b ;
	printf("Enter 2 numbers :");
	a = scanner();
	b = scanner();
	printer(sum_of_two_numbers(a,b), a, b);
	return 0;
}


// function to calculate the sum
float sum_of_two_numbers(float a ,float b)
{	
	float c = a + b; 
	return c;
}

// function to print the number
void printer(float x, float a, float b)
{
	printf("The sum of %0.4f and %0.4f is %0.4f", a, b, x);
}

// function to scan the number
float scanner()
{	
	float x ;
	scanf("%f", &x );
    return x;
} 