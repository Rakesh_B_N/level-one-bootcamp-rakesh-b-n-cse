//WAP to find the distance between two point using 4 functions.

#include <stdio.h>
#include<math.h>
float scanner();
float distance_calculator(float a, float b, float c, float d);
void distance_printer(float x1, float y1, float x2, float x3, float distance);
float scanner_x1();
float scanner_y1();
float scanner_x2();
float scanner_y2();

int main()
{   
    float x1, y1, x2, y2;
    x1 = scanner_x1();
    y1 = scanner_y1();
    x2 = scanner_x2();
    y2 = scanner_y2();
    distance_printer(x1, y1, x2, y2, distance_calculator(x1, y1, x2, y2) );
    return 0;
}

// function to calculate the distance between two points.
float distance_calculator(float x1, float y1, float x2, float y2)
{ 
    float distance = sqrt( pow((x1-x2),2) + pow((y1-y2), 2) );
    return distance;
}


void distance_printer(float x1, float y1, float x2, float y2, float distance)
{
	printf("\nThe distance between points (%0.4f , %0.4f) and (%0.4f , %0.4f) is %0.4f .", x1, y1, x2, y2, distance);
}

float scanner_x1()
{   
   float number;
   printf("Enter abscissa of the 1st point  : ");
   scanf("%f",&number);
   return number;
}

float scanner_y1()
{   
   float number;
   printf("Enter ordinate of the 1st point  : ");
   scanf("%f",&number);
   return number;
}
float scanner_x2()
{   
   float number;
   printf("Enter abscissa of the 2nd point  : ");
   scanf("%f",&number);
   return number;
}

float scanner_y2()
{   
   float number;
   printf("Enter ordinate of the 2nd point  : ");
   scanf("%f",&number);
   return number;
}