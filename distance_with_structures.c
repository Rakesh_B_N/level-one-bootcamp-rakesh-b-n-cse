//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include<math.h>

struct coordinates
{
    float x, y;
};

typedef struct coordinates Coordinates;
float distance_calculator(Coordinates temp_p1, Coordinates temp_p2);
void distance_printer( Coordinates temp_p1, Coordinates temp_p2, float distance);
Coordinates scanner_1st_point();
Coordinates scanner_2nd_point();

int main()
{  
    Coordinates p1, p2;
    p1 = scanner_1st_point();
    p2 = scanner_2nd_point();
    distance_printer(p1, p2, distance_calculator(p1, p2));
    return 0;
}


// function to calculate the distance between two points.
float distance_calculator( Coordinates temp_p1, Coordinates temp_p2)
{ 
    float distance = sqrt( pow((temp_p1.x-temp_p2.x),2) + pow((temp_p1.y-temp_p2.y), 2));
    return distance;
}

void distance_printer(Coordinates temp_p1, Coordinates temp_p2, float distance)
{
	printf("The distance between points (%0.4f , %0.4f) and (%0.4f , %0.4f) is %0.4f .", temp_p1.x, temp_p1.y, temp_p2.x, temp_p2.y, distance);
}

Coordinates scanner_1st_point()
{   
   Coordinates number;
   printf("Enter abscissa of 1st point  : ");
   scanf("%f",&number.x);
   printf("Enter ordinate of the 1st point  : ");
   scanf("%f",&number.y);
   return number;
}

Coordinates scanner_2nd_point()
{   
   Coordinates number;
   printf("Enter abscissa of 2nd point  : ");
   scanf("%f",&number.x);
   printf("Enter ordinate of the 2nd point  : ");
   scanf("%f",&number.y);
   return number;
}
