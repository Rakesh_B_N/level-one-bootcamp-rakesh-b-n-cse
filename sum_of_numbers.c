//WAP to find the sum of n different numbers using 4 functions
#include <stdio.h>
#include <stdlib.h>
struct Numbers
{
    float list_of_numbers[100];
    int count;
};
typedef struct Numbers numbers;

numbers input();
void output(numbers arr, float sum);
float sum(numbers arr);

int main()
{   
    int n;
    numbers arr = input();
    float total = sum(arr);
    output(arr, total);
}

numbers input()
{   
    numbers arr;
    printf("Enter the number of total count of the numbers to be added : ");
    scanf("%d", &arr.count);
    for(int i = 0; i < arr.count; i++)
        scanf("%f", &arr.list_of_numbers[i]);
    return arr;
}

void output(numbers arr, float sum)
{
    if(arr.count < 1)
    {
        printf("Invalid input, please enter a valid input .");
        return;
    }
    printf("The sum of ");
    
    for(int i = 0; i < arr.count; i++)
    {
       printf("%0.2f ",arr.list_of_numbers[i]);
       
       if((i+1) < arr.count)
           printf("+ ");
    }
    
    printf("is \" %0.2f \" .", sum);
}

float sum(numbers arr)
{
    float sum = 0;
    for(int i = 0; i < arr.count; i++)
        sum += arr.list_of_numbers[i];
    return sum;
}

/*
#include <stdio.h>

float input();
float number_input(int n);
void output(int n, float arr[100], float sum);
float sum(int n, float arr[100]);

int main()
{   
    int n = input();
    float arr[100] = [0];
    arr[100] =  number_input(n);
    float total = sum(n, arr);
    output(n, arr, total);
		return 0;
}

float input()
{   
    int n;
    printf("Enter the number of total count of the numbers to be added : ");
    scanf("%d", &n);
    return n;
}

float number_input(int n)
{
    float numbers[100];
    for(int i = 0; i < n; i++)
	{ 
  	    scanf("%f", &numbers[i]);
	}
	return numbers[100];
}

void output(int n, float arr[100], float sum)
{
    if(n < 1)
    {
        printf("Invalid input, please enter a valid input .");
        return;
    }
    printf("The sum of ");
    
    for(int i = 0; i < n; i++)
    {
       printf("%0.2f ", arr[i]);
       
       if((i+1) < n)
           printf("+ ");
    }
    
    printf("is \" %0.2f \" .", sum);
}

float sum(int n, float arr[100])
{
    float sum = 0;
    for(int i = 0; i < n; i++)
        sum += arr[i];
    return sum;
}*/







