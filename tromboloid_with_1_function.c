//Write a program to find the volume of a tromboloid using one function

#include <stdio.h>

int main() 
{
	float h, b, d;
	printf("Enter h , b , d respetively : ");
	scanf("%f %f %f", &h, &b, &d);
  	float volume = (1/(3*b))*((h * d) + d);
	printf("volume of tromboloid with given dimensions is %0.4f", volume);
	return 0;	
}


