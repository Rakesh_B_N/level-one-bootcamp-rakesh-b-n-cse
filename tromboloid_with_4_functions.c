//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float volume_of_tromboloid(float h ,float b, float d);
void printer(float h , float b, float d, float volume);
float scanner();

int main() 
{
	float h, b, d;
	printf("Enter h , b , d respetively : ");
	h = scanner();
	b = scanner();
	d = scanner();
    printer(h, b, d, volume_of_tromboloid(h, b, d));
	return 0;	
}

// function to calculate the volume of tromboloid
float volume_of_tromboloid(float h ,float b, float d)
{	
	float volume = (1/(3*b))*((h * d) + d);
	return volume;
}

//function to print the Volume
void printer(float h , float b, float d, float volume)
{
	printf("Volume of tromboloid with h = %0.4f, b = %0.4f and d = %0.4f is %0.4f\n", h, b, d, volume);
}

// function to scan the number
float scanner()
{	
	float x ;
	scanf("%f", &x );
    return x;
} 
