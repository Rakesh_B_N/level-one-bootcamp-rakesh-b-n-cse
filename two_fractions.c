//WAP to find the sum of two fractions.
#include <stdio.h>
#include <stdlib.h>

struct fraction
{
  int numerator, denominator;
};
typedef struct fraction fraction;

fraction get_fraction(int n);
void output(fraction f1, fraction f2, fraction f3);
fraction add_fractions(fraction n1, fraction n2);
int hcf(int x, int y);
fraction fraction_simplifier(fraction f3);

int main()
{
   fraction f1, f2, f3;
   f1 = get_fraction(1);
   f2 = get_fraction(2);
   f3 = add_fractions(f1, f2);
   output(f1, f2, fraction_simplifier(f3));
}

fraction get_fraction(int n)
{   
   fraction number;
   printf("Enter integral numerator of the %dst fraction : ",n);
   scanf("%d",&number.numerator);
   printf("Enter integral denominator of the %dst fraction : ",n);
   scanf("%d",&number.denominator);
   return number;
}

fraction fraction_simplifier(fraction f3)
{
    int numerator = f3.numerator;
    int denominator = f3.denominator;
    int HCF = hcf(numerator, denominator);
    f3.numerator = numerator/HCF;
    f3.denominator = denominator/HCF;
    return f3;
}

// funtion to print result.
void output(fraction f1, fraction f2, fraction f3)
{
    printf("\nThe sum of %d/%d and %d/%d is %d/%d", f1.numerator, f1.denominator, f2.numerator, f2.denominator, f3.numerator, f3.denominator);
}

fraction add_fractions(fraction n1, fraction n2)
{
	fraction f3;
	f3.numerator = (n1.numerator)*(n2.denominator)+(n2.numerator)*(n1.denominator);
	f3.denominator = (n1.denominator)*(n2.denominator);
	return f3;
}

// function to find hcf of two numbers.
int hcf(int x, int y)
{   
    int temp;
    while(y != 0)
    {
        temp = y;
        y = x % y;
        x = temp;
    }
    return x;
}

/*int hcf(int x, int y)
{   
    while(x != y)
    {
        if(x > y)
            x = x - y;
        else
            y = y - x;
    }
    return x;
}*/

// function to find hcf of two numbers.
/*int hcf(int x, int y) 
{
    if (y != 0)
  	  return hcf(y, x % y);
    else
  	  return x;
}*/

// function to find hcf of two numbers.
/*int hcf(int x, int y) 
{
    for(i=1; i <= n1 && i <= n2; ++i)
    {
        if(n1%i==0 && n2%i==0)
            gcd = i;
    }
}*/


